akira (0.0.16-2) unstable; urgency=medium

  * update copyright info:
    + use Reference field (not License-Reference);
      tighten lintian overrides
    + update coverage
  * drop patch 2001 to use approximate theme;
    depend on elementary-icon-theme
    (not elementary-xfce-icon-theme)

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 15 Mar 2022 21:09:24 +0100

akira (0.0.16-1) unstable; urgency=medium

  [ upstream ]
  * new pre-release

  [ Jonas Smedegaard ]
  * declare compliance with Debian Policy 4.6.0
  * tighten build-dependency on libgranite-dev

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 24 Aug 2021 19:51:22 +0200

akira (0.0.15-1) experimental; urgency=medium

  [ upstream ]
  * new pre-release

  [ Jonas Smedegaard ]
  * unfuzz patches
  * copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 21 Jul 2021 16:07:25 +0200

akira (0.0.14-1~exp1) experimental; urgency=medium

  [ upstream ]
  * new pre-release

  [ Jonas Smedegaard ]
  * copyright info: update coverage
  * unfuzz patch 2001
  * add patch 2002 to use goocanvas 2.0 (not goocanvas 3.0)

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 17 May 2021 09:07:40 +0200

akira (0.0.13-3) unstable; urgency=medium

  * add patch 2001 to use icon theme elementary-xfce,
    thanks to François Téchené;
    depend on elementary-xfce-icon-theme
  * declare compliance with Debian Policy 4.5.1
  * update git-buildpage settings: add usage comment
  * use debhelper compatibility level 13 (not 12)

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 23 Dec 2020 18:24:32 +0100

akira (0.0.13-2) unstable; urgency=medium

  * relax to build-depend unversioned on valac
    (needed version satisfied even in oldstable)
  * fix set dh option --builddirectory
    (not override dh_auto_configure),
    to apply Debian-specific build options;
    closes: bug#971006, thanks to Helmut Grohne

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 26 Sep 2020 12:28:10 +0200

akira (0.0.13-1) unstable; urgency=medium

  [ upstream ]
  * new pre-release

  [ Jonas Smedegaard ]
  * watch: track git tags (not commits)
  * simplify source script copyright-check
  * copyright: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 25 Sep 2020 12:33:37 +0200

akira (0~20200403~8a0f1c9-2) experimental; urgency=medium

  * drop needless build-dependencies
    on libevdev-dev libgtksourceview-3.0-dev libgudev-1.0-dev;
    closes: bug#955734, thanks to Pino Toscano

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 04 Apr 2020 13:57:49 +0200

akira (0~20200403~8a0f1c9-1) experimental; urgency=medium

  [ upstream ]
  * new git snapshot

  [ Jonas Smedegaard ]
  * drop patch 1001 now fixed upstream
  * use debhelper compatibility level 12 (not 9);
    build-depend on debhelper-compat (not debhelper)
  * declare compliance with Debian Policy 4.5.0
  * update upstream homepage and source URLs
  * copyright: extend coverage
  * tighten build-dependency on libgranite-dev
  * run tests in virtual X11,
    and tolerate failures when targeted experimental;
    build-depend on xauth xvfb

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 04 Apr 2020 11:47:33 +0200

akira (0~20191012~cab952d-1) experimental; urgency=medium

  [ upstream ]
  * New git snapshot.

  [ Jonas Smedegaard ]
  * Unfuzz patch 1001.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 04 Dec 2019 20:51:17 +0100

akira (0~20190929~7e9b94b-1) experimental; urgency=medium

  [ upstream ]
  * New git snapshot.

  [ Jonas Smedegaard ]
  * Update watch file: Use git mode. Rewrite usage comment.
  * Declare compliance with Debian Policy 4.4.1.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Oct 2019 11:18:52 +0200

akira (0~20190724~88f9ac5-1) experimental; urgency=medium

  [ upstream ]
  * New git snapshot.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Fix source URLs.
    + Extend upstream copyright and licensing.
  * Declare compliance with Debian Policy 4.4.0.
  * Update lintian overrides regarding license fields.
  * Build-depend on libarchive-dev libgoocanvas-2.0-dev.
  * Cherry-pick patch from upstream WIP branch to work with newer vala.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 26 Jul 2019 16:29:46 -0300

akira (0~20181010~144fd19-1) experimental; urgency=low

  * Initial Release.
    Closes: Bug#919678.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 18 Jan 2019 15:42:38 +0100
